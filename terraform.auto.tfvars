ssh_pub          = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDmj+tLA+k8osR1h0llh+xgyM5YHgQ+xdiDCbRnyHxCTvI4GOkF1MPmJ7zANtGELUT+UQ4lniyjHpGWFggrsbuvyMXZN8e2XZSpl5g4Rbe5hu/WXnVfCJXvayCa3W7/L8Dy8ECXzlsMA4VjcPIWJhrN763Ysu7e2yfaSpPyZjDrcgG4MnrwGAX/40Arbyz6HqkDoWHcAAUaVFN5c0IwLwnIk5uXehNK4/EW6lpiD/Qgy8nKB6GX1fOhTNs6KfyfJTYCwKQ+0IQ38s+UNcHqO/kn2V7U01KnVV18mXeIUhvi0C/ZBF6QcKs/8rIQWsb1EzMb82Vhn0WFRCYQIzjH9AU/DNtoH2f1Ftlz2eXZ8KAPr8I+XHIB/MoacYLJM8u68A1ccKAgrq2Utpb7zKX3rQ+l/3S4mzKbWA4Va+A2WuNNMWuHpzIIln7+2yh6niui59Um546r6yDD0YArOjAnidwvu5hoj4EHpEbtYnNszFCEgvmMzkRqXsV+TR3VZjGz3s0= feliperibeiro@MacBook-Pro-de-Felipe.local"
project          = "golden-totality-301321"
account_id_k8s   = "kubernetesmanager2"
k8s_name         = "gke-cluster"
region           = "us-central1"
initial_node     = 1
shape_k8s        = "e2-small"
K8s_name_cluster = "k8s-study"

instances_iaas = {
  "Server_Debian" = {
    hostname   = "debian-server-001"
    shape_type = "e2-micro"
    image      = "debian-cloud/debian-9"
  },
}

instance_enabled = false
k8s_enabled      = false
