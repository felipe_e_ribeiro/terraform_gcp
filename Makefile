cnf ?= .env
include $(env)
export $(shell sed 's/=.*//' $(cnf))

TAG=$(shell git describe --tags --abbrev=0)
GIT_COMMIT=$(shell git log -1 --format=%h)
TERRAFORM_VERSION=1.0.7

#HELP

.PHONY: help
help: ## Abaixo seguem as opcoes validas para o makefile:
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

terraform-init: ## Inicia o processo de init para o terraform;
	sudo docker run --rm -v $$PWD:/app -v $$HOME/.ssh:/root/.ssh/  -w /app/ --env-file=.env hashicorp/terraform:$(TERRAFORM_VERSION) init -upgrade=true

terraform-plan: ## Realiza o processo de plan;
	sudo docker run --rm -v $$PWD:/app -v $$HOME/.ssh:/root/.ssh/   -w /app/ --env-file=.env hashicorp/terraform:$(TERRAFORM_VERSION) plan -out=tfplan_out

terraform-apply: ## Aplica processo de apply, realizando as mudancas configuradas;
	docker run --rm -v $$PWD:/app -v $$HOME/.ssh:/root/.ssh/   -w /app/ --env-file=.env hashicorp/terraform:$(TERRAFORM_VERSION) apply "tfplan_out" ; rm -f tfplan_out

terraform-console: ## Analista informacao de output e variaveis
	docker run --rm -it -v $$PWD:/app -v $$HOME/.ssh:/root/.ssh/   -w /app/ --env-file=.env hashicorp/terraform:$(TERRAFORM_VERSION) console

terraform-bash: ## Entra na instancia para outros CLI do terraform
	docker run --rm -it -v $$PWD:/app -v $$HOME/.ssh:/root/.ssh/   -w /app/ --env-file=.env --entrypoint="" hashicorp/terraform:$(TERRAFORM_VERSION) sh

terraform-destroy: ## Destroi o ambiente - Cuidado, ele possui auto-aprove.
	docker run --rm -v $$PWD:/app -v $$HOME/.ssh:/root/.ssh/   -w /app/ --env-file=.env hashicorp/terraform:$(TERRAFORM_VERSION) destroy -auto-approve

