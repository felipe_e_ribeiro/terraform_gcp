provider "google" {
  project = var.project
  region  = "us-central1"
  zone    = "us-central1-c"
}

terraform {
  backend "gcs" {
    bucket = "tf-state-prod-felipeeduribeiro"
    prefix = "terraform/state"
  }
}

module "instance" {
  source           = "./Terraform_GoogleCloud_Instance/"
  ssh_key          = var.ssh_key
  instances_iaas   = var.instances_iaas
  instance_enabled = var.instance_enabled
}

module "gke" {
  source           = "./Terraform_GoogleCloud_GKE/"
  region           = var.region
  initial_node     = var.initial_node
  shape_k8s        = var.shape_k8s
  account_id_k8s   = var.account_id_k8s
  k8s_name         = var.k8s_name
  k8s_enabled      = var.k8s_enabled
  K8s_name_cluster = var.K8s_name_cluster
}

module "kubectl" {
  source       = "./Terraform_GoogleCloud_Kubectl/"
  access_token = module.gke.access_token
  endpoint     = module.gke.endpoint
  ca_cert      = module.gke.ca_cert[0]
}