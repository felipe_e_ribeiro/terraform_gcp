variable "ssh_pub" {
  type = string
}

variable "project" {
  type = string
}

variable "region" {
  type = string
}

variable "initial_node" {
  type = number
}

variable "shape_k8s" {
  type = string
}

variable "account_id_k8s" {
  type = string
}

variable "k8s_name" {
  type = string
}

variable "K8s_name_cluster" {
  type = string
}

variable "instances_iaas" {
  type = map(object({
    hostname   = string
    shape_type = string
    image      = string
  }))
}

variable "ssh_key" {
  type = string
}

variable "instance_enabled" {
  type = bool
}

variable "k8s_enabled" {
  type = bool
}