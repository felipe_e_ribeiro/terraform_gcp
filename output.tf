output "instances_info" {
  value = { for k, v in module.instance.instances_output : k => v.network_interface if var.instance_enabled == true }
}

output "k8s_endpoint" {
  value = module.gke.k8s.endpoint
}

#output "k8s_url_group" {
#  value = { for k, v in module.gke.k8s.node_pool[0].instance_group_urls : k => v if var.k8s_enabled == true }
#}