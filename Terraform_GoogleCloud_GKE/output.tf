
output "k8s" {
  value = data.google_container_cluster.cluster_k8s
  depends_on = [
    google_container_node_pool.primary_preemptible_nodes,
  ]
}

output "access_token" {
  value = data.google_client_config.current.access_token
  depends_on = [
    google_container_node_pool.primary_preemptible_nodes,
  ]
}

output "endpoint" {
  value = data.google_container_cluster.cluster_k8s.endpoint
  depends_on = [
    google_container_node_pool.primary_preemptible_nodes,
  ]
}

output "ca_cert" {
  value = { for k, v in data.google_container_cluster.cluster_k8s.master_auth : k => v.cluster_ca_certificate }
  #value = data.google_container_cluster.cluster_k8s.master_auth.0.cluster_ca_certificate
  depends_on = [
    google_container_node_pool.primary_preemptible_nodes,
  ]
}