variable "region" {
  type = string
}

variable "initial_node" {
  type = number
}

variable "shape_k8s" {
  type = string
}

variable "K8s_name_cluster" {
  type = string
}

variable "account_id_k8s" {
  type = string
}

variable "k8s_name" {
  type = string
}

variable "k8s_enabled" {
  type = bool
}