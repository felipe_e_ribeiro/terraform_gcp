resource "google_container_cluster" "primary" {
  count    = var.k8s_enabled ? 1 : 0
  name     = var.k8s_name
  location = var.region

  remove_default_node_pool = true
  initial_node_count       = var.initial_node
}

resource "google_service_account" "default" {
  count        = var.k8s_enabled ? 1 : 0
  account_id   = var.account_id_k8s
  display_name = "Service Account"
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  count      = var.k8s_enabled ? 1 : 0
  name       = var.K8s_name_cluster
  location   = var.region
  cluster    = google_container_cluster.primary[0].name
  node_count = var.initial_node

  node_config {
    preemptible  = true
    machine_type = var.shape_k8s

    service_account = google_service_account.default[0].email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}

data "google_client_config" "current" {
}

data "google_container_cluster" "cluster_k8s" {
  name     = var.k8s_name
  location = var.region
  depends_on = [
    google_container_node_pool.primary_preemptible_nodes,
  ]
}