
resource "google_compute_instance" "vm_instance" {
  for_each     = var.instance_enabled ? var.instances_iaas : tomap({})
  name         = each.value.hostname
  machine_type = each.value.shape_type

  boot_disk {
    initialize_params {
      image = each.value.image
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  metadata = {
    ssh-keys = var.ssh_key
  }
}

output "instances_output" {
  value = google_compute_instance.vm_instance
}