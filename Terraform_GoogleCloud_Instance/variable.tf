variable "instances_iaas" {
  type = map(object({
    hostname   = string
    shape_type = string
    image      = string
  }))
}

variable "ssh_key" {
  type = string
}

variable "instance_enabled" {
  type = bool
}