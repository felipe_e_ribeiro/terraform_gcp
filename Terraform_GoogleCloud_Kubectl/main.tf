terraform {
  required_version = ">= 0.13"

  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
}

provider "kubectl" {
  load_config_file       = false
  host                   = "https://${var.endpoint}"
  token                  = var.access_token
  cluster_ca_certificate = base64decode(var.ca_cert)
}

resource "kubectl_manifest" "deployment" {
  yaml_body = file("${path.module}/deployments_app.yml")
}

resource "kubectl_manifest" "my_service" {
  yaml_body = file("${path.module}/my_service.yaml")
}

resource "kubectl_manifest" "ingress" {
  yaml_body = file("${path.module}/ingress.yaml")
}