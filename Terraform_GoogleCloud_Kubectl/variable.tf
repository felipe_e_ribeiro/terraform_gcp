variable "access_token" {
  type = string
}

variable "endpoint" {
  type = string
}

variable "ca_cert" {
  type = string
}